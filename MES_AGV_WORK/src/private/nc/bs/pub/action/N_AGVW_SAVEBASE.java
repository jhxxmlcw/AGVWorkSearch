package nc.bs.pub.action;

import nc.bs.framework.common.NCLocator;
import nc.bs.pubapp.pf.action.AbstractPfAction;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.jcom.lang.StringUtil;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

import nc.bs.uapbd.mes_agv_work.plugin.bpplugin.Mes_agv_workPluginPoint;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.itf.uapbd.IMes_agv_workMaintain;

public class N_AGVW_SAVEBASE extends AbstractPfAction<Mes_agv_workBillVO> {

	@Override
	protected CompareAroundProcesser<Mes_agv_workBillVO> getCompareAroundProcesserWithRules(
			Object userObj) {
		CompareAroundProcesser<Mes_agv_workBillVO> processor = null;
		Mes_agv_workBillVO[] clientFullVOs = (Mes_agv_workBillVO[]) this.getVos();
		if (!StringUtil.isEmptyWithTrim(clientFullVOs[0].getParentVO()
				.getPrimaryKey())) {
			processor = new CompareAroundProcesser<Mes_agv_workBillVO>(
					Mes_agv_workPluginPoint.SCRIPT_UPDATE);
		} else {
			processor = new CompareAroundProcesser<Mes_agv_workBillVO>(
					Mes_agv_workPluginPoint.SCRIPT_INSERT);
		}
		// TODO 在此处添加前后规则
		IRule<Mes_agv_workBillVO> rule = null;

		return processor;
	}

	@Override
	protected Mes_agv_workBillVO[] processBP(Object userObj,
			Mes_agv_workBillVO[] clientFullVOs, Mes_agv_workBillVO[] originBills) {

		Mes_agv_workBillVO[] bills = null;
		try {
			IMes_agv_workMaintain operator = NCLocator.getInstance()
					.lookup(IMes_agv_workMaintain.class);
			if (!StringUtil.isEmptyWithTrim(clientFullVOs[0].getParentVO()
					.getPrimaryKey())) {
				bills = operator.update(clientFullVOs);
			} else {
				bills = operator.insert(clientFullVOs);
			}
		} catch (BusinessException e) {
			ExceptionUtils.wrappBusinessException(e.getMessage());
		}
		return bills;
	}
}
