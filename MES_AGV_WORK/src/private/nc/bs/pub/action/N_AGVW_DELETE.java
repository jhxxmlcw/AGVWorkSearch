package nc.bs.pub.action;

import nc.bs.framework.common.NCLocator;
import nc.bs.pubapp.pf.action.AbstractPfAction;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

import nc.bs.uapbd.mes_agv_work.plugin.bpplugin.Mes_agv_workPluginPoint;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.itf.uapbd.IMes_agv_workMaintain;

public class N_AGVW_DELETE extends AbstractPfAction<Mes_agv_workBillVO> {

	@Override
	protected CompareAroundProcesser<Mes_agv_workBillVO> getCompareAroundProcesserWithRules(
			Object userObj) {
		CompareAroundProcesser<Mes_agv_workBillVO> processor = new CompareAroundProcesser<Mes_agv_workBillVO>(
				Mes_agv_workPluginPoint.SCRIPT_DELETE);
		// TODO 在此处添加前后规则
		return processor;
	}

	@Override
	protected Mes_agv_workBillVO[] processBP(Object userObj,
			Mes_agv_workBillVO[] clientFullVOs, Mes_agv_workBillVO[] originBills) {
		IMes_agv_workMaintain operator = NCLocator.getInstance().lookup(
				IMes_agv_workMaintain.class);
		try {
			operator.delete(clientFullVOs);
		} catch (BusinessException e) {
			ExceptionUtils.wrappBusinessException(e.getMessage());
		}
		return clientFullVOs;
	}

}
