package nc.bs.uapbd.mes_agv_work.ace.bp;

import nc.bs.uapbd.mes_agv_work.plugin.bpplugin.Mes_agv_workPluginPoint;
import nc.impl.pubapp.pattern.data.bill.template.UpdateBPTemplate;
import nc.impl.pubapp.pattern.rule.ICompareRule;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;

/**
 * 修改保存的BP
 * 
 */
public class AceMes_agv_workUpdateBP {

  public Mes_agv_workBillVO[] update(Mes_agv_workBillVO[] bills, Mes_agv_workBillVO[] originBills) {

    		    // 调用修改模板
        UpdateBPTemplate<Mes_agv_workBillVO> bp = new UpdateBPTemplate<Mes_agv_workBillVO>(Mes_agv_workPluginPoint.UPDATE);

        // 执行前规则
        this.addBeforeRule(bp.getAroundProcesser());
        return bp.update(bills, originBills);
  }
  private void addBeforeRule(CompareAroundProcesser<Mes_agv_workBillVO> processer) {
   IRule<Mes_agv_workBillVO> rule=null;
					  				  				   				     rule = new nc.bs.pubapp.pub.rule.FillUpdateDataRule();
				    				   				    				     				    				   				   				   				    				     processer.addBeforeRule(rule);
				    				     				  				   				     ICompareRule<Mes_agv_workBillVO> ruleCom = new nc.bs.pubapp.pub.rule.UpdateBillCodeRule();
				    				   				    				     ((nc.bs.pubapp.pub.rule.UpdateBillCodeRule)ruleCom).setCbilltype("AGVW");
				    				   				    				     ((nc.bs.pubapp.pub.rule.UpdateBillCodeRule)ruleCom).setCodeItem("vbillcode");
				    				   				    				     ((nc.bs.pubapp.pub.rule.UpdateBillCodeRule)ruleCom).setGroupItem("pk_group");
				    				   				    				     ((nc.bs.pubapp.pub.rule.UpdateBillCodeRule)ruleCom).setOrgItem("pk_org");
				    				   				   				   				    				     processer.addBeforeRule(ruleCom);
				    				     				  				   				     rule = new nc.bs.pubapp.pub.rule.FieldLengthCheckRule();
				    				   				    				     				    				   				   				   				    				     processer.addBeforeRule(rule);
				    				       }

}
