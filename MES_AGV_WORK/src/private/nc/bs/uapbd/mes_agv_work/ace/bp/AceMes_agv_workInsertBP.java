package nc.bs.uapbd.mes_agv_work.ace.bp;

import nc.bs.uapbd.mes_agv_work.plugin.bpplugin.Mes_agv_workPluginPoint;
import nc.impl.pubapp.pattern.data.bill.template.InsertBPTemplate;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.impl.pubapp.pattern.rule.processer.AroundProcesser;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;

/**
 * 标准单据新增BP
 */
public class AceMes_agv_workInsertBP {

  public Mes_agv_workBillVO[] insert(Mes_agv_workBillVO[] bills) {

    InsertBPTemplate<Mes_agv_workBillVO> bp =
        new InsertBPTemplate<Mes_agv_workBillVO>(Mes_agv_workPluginPoint.INSERT);
    this.addBeforeRule(bp.getAroundProcesser());
    return bp.insert(bills);
  }
  /**
   * 新增前规则
   * 
   * @param processor
   */
  private void addBeforeRule(AroundProcesser<Mes_agv_workBillVO> processer) {
   IRule<Mes_agv_workBillVO> rule=null;
				  				   				     rule = new nc.bs.pubapp.pub.rule.FillInsertDataRule();
				   				   				    				   				   
				   				     processer.addBeforeRule(rule);
				   				   
  				   				     rule = new nc.bs.pubapp.pub.rule.CreateBillCodeRule();
				   				   				    				     ((nc.bs.pubapp.pub.rule.CreateBillCodeRule)rule).setCbilltype("AGVW");
				    				   				    				     ((nc.bs.pubapp.pub.rule.CreateBillCodeRule)rule).setCodeItem("vbillcode");
				    				   				    				     ((nc.bs.pubapp.pub.rule.CreateBillCodeRule)rule).setGroupItem("pk_group");
				    				   				    				     ((nc.bs.pubapp.pub.rule.CreateBillCodeRule)rule).setOrgItem("pk_org");
				    				   				   
				   				     processer.addBeforeRule(rule);
				   				   
  				   				     rule = new nc.bs.pubapp.pub.rule.FieldLengthCheckRule();
				   				   				    				   				   
				   				     processer.addBeforeRule(rule);
				   				   
    }
}
