package nc.bs.uapbd.mes_agv_work.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.vo.pub.VOStatus;

/**
 * 标准单据弃审的BP
 */
public class AceMes_agv_workUnApproveBP {

	public Mes_agv_workBillVO[] unApprove(Mes_agv_workBillVO[] clientBills,
			Mes_agv_workBillVO[] originBills) {
		for (Mes_agv_workBillVO clientBill : clientBills) {
			clientBill.getParentVO().setStatus(VOStatus.UPDATED);
		}
		BillUpdate<Mes_agv_workBillVO> update = new BillUpdate<Mes_agv_workBillVO>();
		Mes_agv_workBillVO[] returnVos = update.update(clientBills, originBills);
		return returnVos;
	}
}
