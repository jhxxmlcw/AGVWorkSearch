package nc.bs.uapbd.mes_agv_work.ace.bp;

import nc.bs.uapbd.mes_agv_work.plugin.bpplugin.Mes_agv_workPluginPoint;
import nc.impl.pubapp.pattern.data.bill.template.DeleteBPTemplate;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.impl.pubapp.pattern.rule.processer.AroundProcesser;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;

/**
 * ��׼����ɾ��BP
 */
public class AceMes_agv_workDeleteBP {

  public void delete(Mes_agv_workBillVO[] bills) {

      DeleteBPTemplate<Mes_agv_workBillVO> bp =
          new DeleteBPTemplate<Mes_agv_workBillVO>(Mes_agv_workPluginPoint.DELETE);
      bp.delete(bills);
  }
}
