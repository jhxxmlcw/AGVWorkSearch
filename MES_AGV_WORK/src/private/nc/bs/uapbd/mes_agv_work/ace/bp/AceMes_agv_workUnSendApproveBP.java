package nc.bs.uapbd.mes_agv_work.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.vo.pub.VOStatus;
import nc.vo.pub.pf.BillStatusEnum;

/**
 * 标准单据收回的BP
 */
public class AceMes_agv_workUnSendApproveBP {

	public Mes_agv_workBillVO[] unSend(Mes_agv_workBillVO[] clientBills,
			Mes_agv_workBillVO[] originBills) {
		// 把VO持久化到数据库中
		this.setHeadVOStatus(clientBills);
		BillUpdate<Mes_agv_workBillVO> update = new BillUpdate<Mes_agv_workBillVO>();
		Mes_agv_workBillVO[] returnVos = update.update(clientBills, originBills);
		return returnVos;
	}

	private void setHeadVOStatus(Mes_agv_workBillVO[] clientBills) {
		for (Mes_agv_workBillVO clientBill : clientBills) {
			clientBill.getParentVO().setAttributeValue("${vmObject.billstatus}",
					BillStatusEnum.FREE.value());
			clientBill.getParentVO().setStatus(VOStatus.UPDATED);
		}
	}
}
