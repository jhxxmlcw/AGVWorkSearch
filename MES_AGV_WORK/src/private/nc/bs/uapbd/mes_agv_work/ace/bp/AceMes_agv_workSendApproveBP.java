package nc.bs.uapbd.mes_agv_work.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.vo.pub.VOStatus;
import nc.vo.pub.pf.BillStatusEnum;

/**
 * 标准单据送审的BP
 */
public class AceMes_agv_workSendApproveBP {
	/**
	 * 送审动作
	 * 
	 * @param vos
	 *            单据VO数组
	 * @param script
	 *            单据动作脚本对象
	 * @return 送审后的单据VO数组
	 */

	public Mes_agv_workBillVO[] sendApprove(Mes_agv_workBillVO[] clientBills,
			Mes_agv_workBillVO[] originBills) {
		for (Mes_agv_workBillVO clientFullVO : clientBills) {
			clientFullVO.getParentVO().setAttributeValue("${vmObject.billstatus}",
					BillStatusEnum.COMMIT.value());
			clientFullVO.getParentVO().setStatus(VOStatus.UPDATED);
		}
		// 数据持久化
		Mes_agv_workBillVO[] returnVos = new BillUpdate<Mes_agv_workBillVO>().update(
				clientBills, originBills);
		return returnVos;
	}
}
