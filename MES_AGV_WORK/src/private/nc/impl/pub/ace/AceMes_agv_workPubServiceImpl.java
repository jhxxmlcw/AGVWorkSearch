package nc.impl.pub.ace;

import nc.bs.uapbd.mes_agv_work.ace.bp.AceMes_agv_workDeleteBP;
import nc.bs.uapbd.mes_agv_work.ace.bp.AceMes_agv_workInsertBP;
import nc.bs.uapbd.mes_agv_work.ace.bp.AceMes_agv_workUpdateBP;
import nc.impl.pubapp.pattern.data.bill.tool.BillTransferTool;
		import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.impl.pubapp.pattern.data.bill.BillLazyQuery;
import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.pub.BusinessException;
import nc.vo.pub.VOStatus;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;
import nc.vo.pubapp.query2.sql.process.QuerySchemeProcessor;
import nc.impl.pubapp.pattern.database.DataAccessUtils;
		import nc.vo.pubapp.pattern.data.IRowSet;
import nc.vo.pubapp.bill.pagination.PaginationTransferObject;
import nc.vo.pubapp.bill.pagination.util.PaginationUtils;
import nc.impl.pubapp.pattern.data.bill.BillQuery;

public abstract class AceMes_agv_workPubServiceImpl {
    //新增
    public Mes_agv_workBillVO[] pubinsertBills(Mes_agv_workBillVO[] vos) throws BusinessException {
        try {
          // 数据库中数据和前台传递过来的差异VO合并后的结果
          BillTransferTool<Mes_agv_workBillVO> transferTool =new BillTransferTool<Mes_agv_workBillVO>(vos);
          Mes_agv_workBillVO[] mergedVO = transferTool.getClientFullInfoBill();

          // 调用BP
          AceMes_agv_workInsertBP action = new AceMes_agv_workInsertBP();
          Mes_agv_workBillVO[] retvos = action.insert(mergedVO);
          // 构造返回数据
          return transferTool.getBillForToClient(retvos);
        }catch (Exception e) {
          ExceptionUtils.marsh(e);
        }
        return null;
    }
    //删除
    public void pubdeleteBills(Mes_agv_workBillVO[] vos) throws BusinessException {
        try {
			          // 加锁 比较ts
          BillTransferTool<Mes_agv_workBillVO> transferTool = new BillTransferTool<Mes_agv_workBillVO>(vos);
          Mes_agv_workBillVO[] fullBills = transferTool.getClientFullInfoBill();
          AceMes_agv_workDeleteBP deleteBP = new AceMes_agv_workDeleteBP();
          deleteBP.delete(fullBills);
        }catch (Exception e) {
          ExceptionUtils.marsh(e);
        }
    }
  	  //修改
    public Mes_agv_workBillVO[] pubupdateBills(Mes_agv_workBillVO[] vos) throws BusinessException {
        try {
    	      //加锁 + 检查ts
		          BillTransferTool<Mes_agv_workBillVO> transTool = new BillTransferTool<Mes_agv_workBillVO>(vos);
		          //补全前台VO
          Mes_agv_workBillVO[] fullBills = transTool.getClientFullInfoBill();
	          //获得修改前vo
          Mes_agv_workBillVO[] originBills = transTool.getOriginBills();
          // 调用BP
          AceMes_agv_workUpdateBP bp = new AceMes_agv_workUpdateBP();
          Mes_agv_workBillVO[] retBills = bp.update(fullBills, originBills);
          // 构造返回数据
          retBills = transTool.getBillForToClient(retBills);
          return retBills;
        }catch (Exception e) {
          ExceptionUtils.marsh(e);
        }
          return null;
     }


  public Mes_agv_workBillVO[] pubquerybills(IQueryScheme queryScheme)
      throws BusinessException {
    Mes_agv_workBillVO[] bills = null;
    try {
      this.preQuery(queryScheme);
      BillLazyQuery<Mes_agv_workBillVO> query =
          new BillLazyQuery<Mes_agv_workBillVO>(Mes_agv_workBillVO.class);
      bills = query.query(queryScheme, null);
    }
    catch (Exception e) {
      ExceptionUtils.marsh(e);
    }
    return bills;
  }

	  /**
   * 由子类实现，查询之前对queryScheme进行加工，加入自己的逻辑
   * 
   * @param queryScheme
   */
  protected void preQuery(IQueryScheme queryScheme) {
    // 查询之前对queryScheme进行加工，加入自己的逻辑
  }

				
}