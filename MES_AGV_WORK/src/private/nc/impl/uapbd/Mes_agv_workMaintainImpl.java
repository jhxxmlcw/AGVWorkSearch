package nc.impl.uapbd;

import nc.impl.pub.ace.AceMes_agv_workPubServiceImpl;
import nc.impl.pubapp.pattern.data.bill.tool.BillTransferTool;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.impl.pubapp.pattern.data.bill.BillLazyQuery;
import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;
import nc.vo.pubapp.query2.sql.process.QuerySchemeProcessor;
import nc.impl.pubapp.pattern.database.DataAccessUtils;
import nc.vo.pubapp.pattern.data.IRowSet;
import nc.vo.pubapp.bill.pagination.PaginationTransferObject;
import nc.vo.pubapp.bill.pagination.util.PaginationUtils;
import nc.impl.pubapp.pattern.data.bill.BillQuery;

public class Mes_agv_workMaintainImpl extends AceMes_agv_workPubServiceImpl implements nc.itf.uapbd.IMes_agv_workMaintain {

      @Override
    public void delete(Mes_agv_workBillVO[] vos) throws BusinessException {
        super.pubdeleteBills(vos);
    }
  
      @Override
    public Mes_agv_workBillVO[] insert(Mes_agv_workBillVO[] vos) throws BusinessException {
        return super.pubinsertBills(vos);
    }
    
      @Override
    public Mes_agv_workBillVO[] update(Mes_agv_workBillVO[] vos) throws BusinessException {
        return super.pubupdateBills(vos);    
    }
  

  @Override
  public Mes_agv_workBillVO[] query(IQueryScheme queryScheme)
      throws BusinessException {
      return super.pubquerybills(queryScheme);
  }







}
