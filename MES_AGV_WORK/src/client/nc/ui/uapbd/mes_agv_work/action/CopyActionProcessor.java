package nc.ui.uapbd.mes_agv_work.action;

import nc.desktop.ui.ServerTimeProxy;
import nc.ui.pubapp.uif2app.actions.intf.ICopyActionProcessor;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workHVO;
import nc.vo.pub.pf.BillStatusEnum;
import nc.vo.pub.ISuperVO;
import nc.vo.pub.IVOMeta;
import nc.vo.pub.lang.UFDateTime;
import nc.vo.uif2.LoginContext;
/**
 * 单据复制时表头表体处理
 * 
 * @since 6.0
 * @version 2011-7-7 下午02:31:23
 * @author duy
 */
public class CopyActionProcessor implements ICopyActionProcessor<Mes_agv_workBillVO> {

  @Override
  public void processVOAfterCopy(Mes_agv_workBillVO billVO, LoginContext context) {
    this.processHeadVO(billVO, context);
  }

  private void processHeadVO(Mes_agv_workBillVO vo, LoginContext context) {
    UFDateTime datetime = ServerTimeProxy.getInstance().getServerTime();
    Mes_agv_workHVO hvo = vo.getParentVO();
    // 设置空处理
    hvo.setPrimaryKey(null);
    hvo.setVbillcode(null);
    hvo.setModifier(null);
    hvo.setModifiedtime(null);
    hvo.setCreator(null);
    hvo.setCreationtime(null);
    //hvo.setTs(null);
    // 设置默认值
    hvo.setDbilldate(datetime.getDate());
    hvo.setPk_group(context.getPk_group());
    hvo.setPk_org(context.getPk_org());
    hvo.setAttributeValue("fstatusflag", BillStatusEnum.FREE.value());
  }

}
