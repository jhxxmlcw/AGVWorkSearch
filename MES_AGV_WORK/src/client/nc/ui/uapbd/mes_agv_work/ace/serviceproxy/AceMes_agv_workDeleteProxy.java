package nc.ui.uapbd.mes_agv_work.ace.serviceproxy;

import nc.bs.framework.common.NCLocator;
import nc.ui.pubapp.pub.task.ISingleBillService;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
/**
 * 示例单据的删除代理，支持批删除
 * 
 * @since 6.0
 * @version 2011-8-4 上午07:22:35
 * @author duy
 */
public class AceMes_agv_workDeleteProxy implements ISingleBillService<Mes_agv_workBillVO> {

    @Override
    public Mes_agv_workBillVO operateBill(Mes_agv_workBillVO bill) throws Exception {
        nc.itf.uapbd.IMes_agv_workMaintain operator = NCLocator.getInstance().lookup(nc.itf.uapbd.IMes_agv_workMaintain.class);
        operator.delete(new Mes_agv_workBillVO[] {
            bill
        });
        return bill;
    }

}
