package nc.itf.uapbd;

import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.uapbd.mes_agv_work.Mes_agv_workBillVO;
import nc.vo.pub.BusinessException;

public interface IMes_agv_workMaintain {

    public void delete(Mes_agv_workBillVO[] vos) throws BusinessException ;

    public Mes_agv_workBillVO[] insert(Mes_agv_workBillVO[] vos) throws BusinessException ;
  
    public Mes_agv_workBillVO[] update(Mes_agv_workBillVO[] vos) throws BusinessException ;


    public Mes_agv_workBillVO[] query(IQueryScheme queryScheme)
      throws BusinessException;

}
