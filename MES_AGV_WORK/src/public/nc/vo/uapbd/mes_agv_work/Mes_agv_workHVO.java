package nc.vo.uapbd.mes_agv_work;

import nc.vo.pub.IVOMeta;
import nc.vo.pub.SuperVO;
import nc.vo.pub.lang.UFBoolean;
import nc.vo.pub.lang.UFDate;
import nc.vo.pub.lang.UFDateTime;
import nc.vo.pub.lang.UFDouble;
import nc.vo.pubapp.pattern.model.meta.entity.vo.VOMetaFactory;

public class Mes_agv_workHVO extends SuperVO {
/**
*AGV小车编号
*/
public Integer agvno;
/**
*单据编号
*/
public String billid;
/**
*制单时间
*/
public static final String CREATIONTIME="creationtime";
/**
*制单人
*/
public static final String CREATOR="creator";
/**
*单据日期
*/
public static final String DBILLDATE="dbilldate";
/**
*结束主键
*/
public String endpk;
/**
*终点
*/
public String endpos;
/**
*结束时间
*/
public String endtime;
/**
*错误信息
*/
public String errorinfo;
/**
*单据状态
*/
public static final String FSTATUSFLAG="fstatusflag";
/**
*任务类型
*/
public Integer jobtype;
/**
*mes工段
*/
public String mesprocess;
/**
*mo编号
*/
public String mocode;
/**
*最后修改时间
*/
public static final String MODIFIEDTIME="modifiedtime";
/**
*最后修改人
*/
public static final String MODIFIER="modifier";
/**
*需求重量
*/
public Integer needweight;
/**
*实际重量
*/
public Integer nweight;
/**
*剥壳AGV任务表主键
*/
public static final String PK_BK_QS="pk_bk_qs";
/**
*集团
*/
public static final String PK_GROUP="pk_group";
/**
*组织
*/
public static final String PK_ORG="pk_org";
/**
*组织版本
*/
public static final String PK_ORG_V="pk_org_v";
/**
*主rfid号
*/
public String rfid;
/**
*辅rfid号
*/
public String rfid2;
/**
*开始主键
*/
public String startpk;
/**
*起点
*/
public String startpos;
/**
*开始时间
*/
public String starttime;
/**
*时间戳
*/
public static final String TS="ts";
/**
*单据号
*/
public static final String VBILLCODE="vbillcode";
/**
*备注
*/
public static final String VNOTE="vnote";
/**
*任务号
*/
public String wrk_no;
/** 
* 获取AGV小车编号
*
* @return AGV小车编号
*/
public Integer getAgvno () {
return this.agvno;
 } 

/** 
* 设置AGV小车编号
*
* @param agvno AGV小车编号
*/
public void setAgvno ( Integer agvno) {
this.agvno=agvno;
 } 

/** 
* 获取单据编号
*
* @return 单据编号
*/
public String getBillid () {
return this.billid;
 } 

/** 
* 设置单据编号
*
* @param billid 单据编号
*/
public void setBillid ( String billid) {
this.billid=billid;
 } 

/** 
* 获取制单时间
*
* @return 制单时间
*/
public UFDateTime getCreationtime () {
return (UFDateTime) this.getAttributeValue( Mes_agv_workHVO.CREATIONTIME);
 } 

/** 
* 设置制单时间
*
* @param creationtime 制单时间
*/
public void setCreationtime ( UFDateTime creationtime) {
this.setAttributeValue( Mes_agv_workHVO.CREATIONTIME,creationtime);
 } 

/** 
* 获取制单人
*
* @return 制单人
*/
public String getCreator () {
return (String) this.getAttributeValue( Mes_agv_workHVO.CREATOR);
 } 

/** 
* 设置制单人
*
* @param creator 制单人
*/
public void setCreator ( String creator) {
this.setAttributeValue( Mes_agv_workHVO.CREATOR,creator);
 } 

/** 
* 获取单据日期
*
* @return 单据日期
*/
public UFDate getDbilldate () {
return (UFDate) this.getAttributeValue( Mes_agv_workHVO.DBILLDATE);
 } 

/** 
* 设置单据日期
*
* @param dbilldate 单据日期
*/
public void setDbilldate ( UFDate dbilldate) {
this.setAttributeValue( Mes_agv_workHVO.DBILLDATE,dbilldate);
 } 

/** 
* 获取结束主键
*
* @return 结束主键
*/
public String getEndpk () {
return this.endpk;
 } 

/** 
* 设置结束主键
*
* @param endpk 结束主键
*/
public void setEndpk ( String endpk) {
this.endpk=endpk;
 } 

/** 
* 获取终点
*
* @return 终点
*/
public String getEndpos () {
return this.endpos;
 } 

/** 
* 设置终点
*
* @param endpos 终点
*/
public void setEndpos ( String endpos) {
this.endpos=endpos;
 } 

/** 
* 获取结束时间
*
* @return 结束时间
*/
public String getEndtime () {
return this.endtime;
 } 

/** 
* 设置结束时间
*
* @param endtime 结束时间
*/
public void setEndtime ( String endtime) {
this.endtime=endtime;
 } 

/** 
* 获取错误信息
*
* @return 错误信息
*/
public String getErrorinfo () {
return this.errorinfo;
 } 

/** 
* 设置错误信息
*
* @param errorinfo 错误信息
*/
public void setErrorinfo ( String errorinfo) {
this.errorinfo=errorinfo;
 } 

/** 
* 获取单据状态
*
* @return 单据状态
* @see String
*/
public Integer getFstatusflag () {
return (Integer) this.getAttributeValue( Mes_agv_workHVO.FSTATUSFLAG);
 } 

/** 
* 设置单据状态
*
* @param fstatusflag 单据状态
* @see String
*/
public void setFstatusflag ( Integer fstatusflag) {
this.setAttributeValue( Mes_agv_workHVO.FSTATUSFLAG,fstatusflag);
 } 

/** 
* 获取任务类型
*
* @return 任务类型
*/
public Integer getJobtype () {
return this.jobtype;
 } 

/** 
* 设置任务类型
*
* @param jobtype 任务类型
*/
public void setJobtype ( Integer jobtype) {
this.jobtype=jobtype;
 } 

/** 
* 获取mes工段
*
* @return mes工段
*/
public String getMesprocess () {
return this.mesprocess;
 } 

/** 
* 设置mes工段
*
* @param mesprocess mes工段
*/
public void setMesprocess ( String mesprocess) {
this.mesprocess=mesprocess;
 } 

/** 
* 获取mo编号
*
* @return mo编号
*/
public String getMocode () {
return this.mocode;
 } 

/** 
* 设置mo编号
*
* @param mocode mo编号
*/
public void setMocode ( String mocode) {
this.mocode=mocode;
 } 

/** 
* 获取最后修改时间
*
* @return 最后修改时间
*/
public UFDateTime getModifiedtime () {
return (UFDateTime) this.getAttributeValue( Mes_agv_workHVO.MODIFIEDTIME);
 } 

/** 
* 设置最后修改时间
*
* @param modifiedtime 最后修改时间
*/
public void setModifiedtime ( UFDateTime modifiedtime) {
this.setAttributeValue( Mes_agv_workHVO.MODIFIEDTIME,modifiedtime);
 } 

/** 
* 获取最后修改人
*
* @return 最后修改人
*/
public String getModifier () {
return (String) this.getAttributeValue( Mes_agv_workHVO.MODIFIER);
 } 

/** 
* 设置最后修改人
*
* @param modifier 最后修改人
*/
public void setModifier ( String modifier) {
this.setAttributeValue( Mes_agv_workHVO.MODIFIER,modifier);
 } 

/** 
* 获取需求重量
*
* @return 需求重量
*/
public Integer getNeedweight () {
return this.needweight;
 } 

/** 
* 设置需求重量
*
* @param needweight 需求重量
*/
public void setNeedweight ( Integer needweight) {
this.needweight=needweight;
 } 

/** 
* 获取实际重量
*
* @return 实际重量
*/
public Integer getNweight () {
return this.nweight;
 } 

/** 
* 设置实际重量
*
* @param nweight 实际重量
*/
public void setNweight ( Integer nweight) {
this.nweight=nweight;
 } 

/** 
* 获取剥壳AGV任务表主键
*
* @return 剥壳AGV任务表主键
*/
public String getPk_bk_qs () {
return (String) this.getAttributeValue( Mes_agv_workHVO.PK_BK_QS);
 } 

/** 
* 设置剥壳AGV任务表主键
*
* @param pk_bk_qs 剥壳AGV任务表主键
*/
public void setPk_bk_qs ( String pk_bk_qs) {
this.setAttributeValue( Mes_agv_workHVO.PK_BK_QS,pk_bk_qs);
 } 

/** 
* 获取集团
*
* @return 集团
*/
public String getPk_group () {
return (String) this.getAttributeValue( Mes_agv_workHVO.PK_GROUP);
 } 

/** 
* 设置集团
*
* @param pk_group 集团
*/
public void setPk_group ( String pk_group) {
this.setAttributeValue( Mes_agv_workHVO.PK_GROUP,pk_group);
 } 

/** 
* 获取组织
*
* @return 组织
*/
public String getPk_org () {
return (String) this.getAttributeValue( Mes_agv_workHVO.PK_ORG);
 } 

/** 
* 设置组织
*
* @param pk_org 组织
*/
public void setPk_org ( String pk_org) {
this.setAttributeValue( Mes_agv_workHVO.PK_ORG,pk_org);
 } 

/** 
* 获取组织版本
*
* @return 组织版本
*/
public String getPk_org_v () {
return (String) this.getAttributeValue( Mes_agv_workHVO.PK_ORG_V);
 } 

/** 
* 设置组织版本
*
* @param pk_org_v 组织版本
*/
public void setPk_org_v ( String pk_org_v) {
this.setAttributeValue( Mes_agv_workHVO.PK_ORG_V,pk_org_v);
 } 

/** 
* 获取主rfid号
*
* @return 主rfid号
*/
public String getRfid () {
return this.rfid;
 } 

/** 
* 设置主rfid号
*
* @param rfid 主rfid号
*/
public void setRfid ( String rfid) {
this.rfid=rfid;
 } 

/** 
* 获取辅rfid号
*
* @return 辅rfid号
*/
public String getRfid2 () {
return this.rfid2;
 } 

/** 
* 设置辅rfid号
*
* @param rfid2 辅rfid号
*/
public void setRfid2 ( String rfid2) {
this.rfid2=rfid2;
 } 

/** 
* 获取开始主键
*
* @return 开始主键
*/
public String getStartpk () {
return this.startpk;
 } 

/** 
* 设置开始主键
*
* @param startpk 开始主键
*/
public void setStartpk ( String startpk) {
this.startpk=startpk;
 } 

/** 
* 获取起点
*
* @return 起点
*/
public String getStartpos () {
return this.startpos;
 } 

/** 
* 设置起点
*
* @param startpos 起点
*/
public void setStartpos ( String startpos) {
this.startpos=startpos;
 } 

/** 
* 获取开始时间
*
* @return 开始时间
*/
public String getStarttime () {
return this.starttime;
 } 

/** 
* 设置开始时间
*
* @param starttime 开始时间
*/
public void setStarttime ( String starttime) {
this.starttime=starttime;
 } 

/** 
* 获取时间戳
*
* @return 时间戳
*/
public UFDateTime getTs () {
return (UFDateTime) this.getAttributeValue( Mes_agv_workHVO.TS);
 } 

/** 
* 设置时间戳
*
* @param ts 时间戳
*/
public void setTs ( UFDateTime ts) {
this.setAttributeValue( Mes_agv_workHVO.TS,ts);
 } 

/** 
* 获取单据号
*
* @return 单据号
*/
public String getVbillcode () {
return (String) this.getAttributeValue( Mes_agv_workHVO.VBILLCODE);
 } 

/** 
* 设置单据号
*
* @param vbillcode 单据号
*/
public void setVbillcode ( String vbillcode) {
this.setAttributeValue( Mes_agv_workHVO.VBILLCODE,vbillcode);
 } 

/** 
* 获取备注
*
* @return 备注
*/
public String getVnote () {
return (String) this.getAttributeValue( Mes_agv_workHVO.VNOTE);
 } 

/** 
* 设置备注
*
* @param vnote 备注
*/
public void setVnote ( String vnote) {
this.setAttributeValue( Mes_agv_workHVO.VNOTE,vnote);
 } 

/** 
* 获取任务号
*
* @return 任务号
*/
public String getWrk_no () {
return this.wrk_no;
 } 

/** 
* 设置任务号
*
* @param wrk_no 任务号
*/
public void setWrk_no ( String wrk_no) {
this.wrk_no=wrk_no;
 } 


  @Override
  public IVOMeta getMetaData() {
    return VOMetaFactory.getInstance().getVOMeta("uapbd.Mes_agv_workHVO");
  }
}