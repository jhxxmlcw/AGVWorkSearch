package nc.vo.uapbd.mes_agv_work;

import nc.vo.pubapp.pattern.model.entity.bill.AbstractBill;
import nc.vo.pubapp.pattern.model.meta.entity.bill.BillMetaFactory;
import nc.vo.pubapp.pattern.model.meta.entity.bill.IBillMeta;

@nc.vo.annotation.AggVoInfo(parentVO = "nc.vo.uapbd.mes_agv_work.Mes_agv_workHVO")
public class Mes_agv_workBillVO extends AbstractBill {

  @Override
  public IBillMeta getMetaData() {
    IBillMeta billMeta =BillMetaFactory.getInstance().getBillMeta(Mes_agv_workBillVOMeta.class);
    return billMeta;
  }

  @Override
  public Mes_agv_workHVO getParentVO() {
    return (Mes_agv_workHVO) this.getParent();
  }
}