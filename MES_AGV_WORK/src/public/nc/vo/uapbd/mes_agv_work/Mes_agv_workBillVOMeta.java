package nc.vo.uapbd.mes_agv_work;

import nc.vo.pubapp.pattern.model.meta.entity.bill.AbstractBillMeta;

public class Mes_agv_workBillVOMeta extends AbstractBillMeta {
  public Mes_agv_workBillVOMeta() {
    this.init();
  }
  private void init() {
    this.setParent(nc.vo.uapbd.mes_agv_work.Mes_agv_workHVO.class);
  }
}